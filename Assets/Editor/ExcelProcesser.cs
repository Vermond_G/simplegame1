﻿using UnityEngine;
using System.Collections;
using System.IO;
using UnityEditor;
using System.Xml.Serialization;
using NPOI.HSSF.UserModel;
using NPOI.XSSF.UserModel;
using NPOI.SS.UserModel;

public class ExcelProcesser : AssetPostprocessor {
    private static readonly string filePath = "Assets/Editor/Data/Data.xlsx";
    private static readonly string eventDataPath = "Assets/Resources/Data/EventData.asset";
    private static readonly string characterDataPath = "Assets/Resources/Data/CharacterData.asset";

    static void OnPostprocessAllAssets(string[] importedAssets, string[] deletedAssets, string[] movedAssets, string[] movedFromAssetPaths)
    {

        foreach (string s in importedAssets)
        {
            if (s != filePath) continue;

            EventData edata = (EventData)AssetDatabase.LoadAssetAtPath(eventDataPath, typeof(CharacterData));
            if (edata == null)
            {
                edata = ScriptableObject.CreateInstance<EventData>();
                AssetDatabase.CreateAsset((ScriptableObject)edata, eventDataPath);
                //edata.hideFlags = HideFlags.NotEditable;
            }

            CharacterData cdata = (CharacterData)AssetDatabase.LoadAssetAtPath(characterDataPath, typeof(CharacterData));
            if (cdata == null)
            {
                cdata = ScriptableObject.CreateInstance<CharacterData>();
                AssetDatabase.CreateAsset((ScriptableObject)cdata, characterDataPath);
                //cdata.hideFlags = HideFlags.NotEditable;
            }

            using (FileStream stream = File.Open(filePath, FileMode.Open, FileAccess.Read))
            {
                IWorkbook book = new XSSFWorkbook(stream);
                ISheet sheet = book.GetSheetAt(0);

                EventData.Officer officer = new EventData.Officer();
                officer.minWaitTime = (float)sheet.GetRow(1).GetCell(1).NumericCellValue;
                officer.maxWaitTime = (float)sheet.GetRow(2).GetCell(1).NumericCellValue;
                officer.minWatchTime = (float)sheet.GetRow(3).GetCell(1).NumericCellValue;
                officer.maxWatchTime = (float)sheet.GetRow(4).GetCell(1).NumericCellValue;

                EventData.Request request = new EventData.Request();
                request.minWaitTime = (float)sheet.GetRow(1).GetCell(2).NumericCellValue;
                request.maxWaitTime = (float)sheet.GetRow(2).GetCell(2).NumericCellValue;
                request.minClickCount = (int)sheet.GetRow(5).GetCell(2).NumericCellValue;
                request.maxClickCount = (int)sheet.GetRow(6).GetCell(2).NumericCellValue;


                EventData.PowerUp powerup = new EventData.PowerUp();
                powerup.minWaitTime = (float)sheet.GetRow(1).GetCell(3).NumericCellValue;
                powerup.maxWaitTime = (float)sheet.GetRow(2).GetCell(3).NumericCellValue;
                powerup.minGuagePercent = (float)sheet.GetRow(7).GetCell(3).NumericCellValue;
                powerup.maxGuagePercent = (float)sheet.GetRow(8).GetCell(3).NumericCellValue;
                powerup.bonusPercent = (float)sheet.GetRow(9).GetCell(3).NumericCellValue;



                edata.officer = officer;
                edata.request = request;
                edata.powerUp = powerup;


                sheet = book.GetSheetAt(1);

                cdata.maxRestPoint = (float)sheet.GetRow(1).GetCell(1).NumericCellValue;
                cdata.maxWorkPoint = (float)sheet.GetRow(2).GetCell(1).NumericCellValue;
                cdata.restForSecond = (float)sheet.GetRow(3).GetCell(1).NumericCellValue;
                cdata.restForSecond = (float)sheet.GetRow(4).GetCell(1).NumericCellValue;

                ScriptableObject obj = (ScriptableObject) AssetDatabase.LoadAssetAtPath(characterDataPath, typeof(ScriptableObject));
                EditorUtility.SetDirty(obj);

            }
        }
    }
}