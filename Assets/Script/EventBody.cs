﻿using UnityEngine;
using System.Collections;

public enum EventStatus { Waiting, Playing }

public class EventBody : MonoBehaviour {

    public float maxWaitTime;// = 10;
    public float minWaitTime;// = 20;
    public float waitTime;// = 10;
    
    public bool isEventDoing;
    public bool isNewState = false;
    public EventStatus nowStatus = EventStatus.Waiting;
    
    protected virtual void OnEnable()
    {
        StartCoroutine(mainRoutine());
    }

    IEnumerator mainRoutine()
    {
        while (true)
        {
            isNewState = false;

            //휴식 시간 재설정
            if (nowStatus == EventStatus.Waiting && waitTime <= 0)
            {
                waitTime = Random.Range(minWaitTime, maxWaitTime);
            }

            yield return StartCoroutine(nowStatus.ToString());
        }
    }

    protected void SetState(EventStatus status)
    {
        isNewState = true;
        nowStatus = status;
    }

    protected virtual IEnumerator Waiting()
    {
        do
        {
            yield return null;

            //시간 감소
            waitTime -= Time.deltaTime;

            //시간이 다 되면 이벤트 실행
            if (waitTime <= 0)
            {
                SetState(EventStatus.Playing);
            }
        } while (!isNewState);
    }

    protected virtual IEnumerator Playing()
    {
        yield return null;
    }


}
