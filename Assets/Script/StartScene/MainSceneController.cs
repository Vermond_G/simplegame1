﻿using UnityEngine;
using System.Collections;

public class MainSceneController : MonoBehaviour
{
    public void StartGame()
    {
        Application.LoadLevel("MainGame");
    }

    public void ShowScoreList()
    {
        Application.LoadLevel("ScoreList");
    }
}