﻿using UnityEngine;
using System.Collections;

public class RequestEvent : EventBody
{
    //아이콘 오브젝트
    public GameObject icon;

    public int maxButtonCount;
    public int minButtonCount;
    public int buttonCount;// = 10;

    public int currentButtonCount = 0;

    //ButtonController controller;

    protected override void OnEnable()
    {
        base.OnEnable();

        //버튼 컨트롤러 설정
        //controller = gameObject.GetComponentInParent<ButtonController>();

        //처음에는 아이콘을 가린다
        icon.gameObject.SetActive(false);

        //값 할당
        maxWaitTime = DataManager.Instance.eventData.request.maxWaitTime;
        minWaitTime = DataManager.Instance.eventData.request.minWaitTime;
        waitTime = Random.Range(minWaitTime, maxWaitTime);

        maxButtonCount = DataManager.Instance.eventData.request.maxClickCount;
        minButtonCount = DataManager.Instance.eventData.request.minClickCount;
        buttonCount = Random.Range(minButtonCount, maxButtonCount);

    }


    protected override IEnumerator Playing()
    {
        //버튼 일반 이벤트 무효화
        //controller.SetStatus(WorkStatus.Eventing);
        DataManager.Instance.nowWorkStatus = WorkStatus.Eventing;

        //아이콘 출력
        icon.gameObject.SetActive(true);

        do
        {
            yield return null;

            //조건 만족시 종료
            if (currentButtonCount >= maxButtonCount)
            {
                //초기화
                currentButtonCount = 0;
                icon.gameObject.SetActive(false);

                //상태 변경
                //controller.SetStatus(WorkStatus.Resting);
                DataManager.Instance.nowWorkStatus = WorkStatus.Resting;
                this.SetState(EventStatus.Waiting);
            }

        } while (!isNewState);
    }
}
