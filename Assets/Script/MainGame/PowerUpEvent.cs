﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class PowerUpEvent : EventBody
{
    public Image powerUpImage;

    //랜덤 최대 최소값
    public float mMaxPower;// = 50;
    public float mMinPower;// = 10;

    //실제 값 저장
    public float mGuageMax;// = 10;
    public float mGuageCurrent = 0;

    //원래 정보 저장
    float mLength;

    //보너스 계수
    public float mBounsTime = 1.2f;

    //타 소스 이벤트 접근용
    //ButtonController btnCtr;
    //public GuageController guageCtr;

    protected override void OnEnable()
    {
        base.OnEnable();

        //초기 게이지바 데이터 저장
        mLength = powerUpImage.rectTransform.sizeDelta.x;

        //값 설정
        maxWaitTime = DataManager.Instance.eventData.powerUp.maxWaitTime;
        minWaitTime = DataManager.Instance.eventData.powerUp.minWaitTime;
        waitTime = Random.Range(minWaitTime, maxWaitTime);

        mMaxPower = DataManager.Instance.eventData.powerUp.maxGuagePercent;
        mMinPower = DataManager.Instance.eventData.powerUp.minGuagePercent;
        mGuageCurrent = Random.Range(mMinPower, mMaxPower);

    }

    protected override IEnumerator Playing()
    {
        do {
            yield return null;
        }while (!DataManager.Instance.isRestGuageMax());

        //활성화
        powerUpImage.gameObject.SetActive(true);

        //최대값 구하기
        mGuageMax = Random.Range(mMinPower, mMaxPower);

        //최대값에 맞춰 길이 변경
        RectTransform rt = powerUpImage.rectTransform;
        rt.sizeDelta = new Vector2(mLength * (mGuageMax / 100), rt.sizeDelta.y);

        //work 상태일때에만 아래 진입
        do
        {
            yield return null;
        } while (DataManager.Instance.nowWorkStatus != WorkStatus.Working);

        //보너스 계수 추가
        mBounsTime = DataManager.Instance.nowBonus;
        DataManager.Instance.nowBonus = DataManager.Instance.eventData.powerUp.bonusPercent;

        do
        {
            yield return null;

            //게이지 보너스 추가

            //버튼 누를수록 길이 줄어들도록 한다
            WorkStatus status = DataManager.Instance.nowWorkStatus;

            if (status == WorkStatus.Working)
            {
                mGuageCurrent += 1;
            }
            else if (status == WorkStatus.Resting)
            {
                //휴식 모드로 들어가면 즉시 종료
                break;
            }

            powerUpImage.fillAmount = 1 - (mGuageCurrent / mGuageMax);

        } while (mGuageCurrent < mGuageMax);
        
        //보너스 계수 원래대로
        DataManager.Instance.nowBonus = mBounsTime;

        //비활성화
        powerUpImage.gameObject.SetActive(false);

        //값 초기화
        mGuageCurrent = 0;
        powerUpImage.fillAmount = 1;
        rt.sizeDelta = new Vector2(mLength, rt.sizeDelta.y);

        //상태 변경
        this.SetState(EventStatus.Waiting);
    }

}