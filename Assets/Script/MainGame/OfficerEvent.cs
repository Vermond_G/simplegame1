﻿using UnityEngine;
using System.Collections;

//애니메이션 설정을 위한 열거형 선언
public enum A_direction { Left=0, Up, Right, Down }

public class OfficerEvent : EventBody
{
    public GameObject officer;
    public GameObject balloon;
    public Transform movePoint;
    Transform[] movePoints;

    public float moveSpeed = 1f;
    public int checkPoint = 1;

    Vector2 initPosition;
    Animator animator;

    float minWatchTime;
    float maxWatchTime;
    float watchTime;

    void Awake()
    {
        animator = officer.GetComponentInChildren<Animator>();

        maxWaitTime = DataManager.Instance.eventData.officer.maxWaitTime;
        minWaitTime = DataManager.Instance.eventData.officer.minWaitTime;
        waitTime = Random.Range(minWaitTime, maxWaitTime);

        minWatchTime = DataManager.Instance.eventData.officer.minWatchTime;
        maxWatchTime = DataManager.Instance.eventData.officer.maxWatchTime;
    }

    protected override void OnEnable()
    {
        base.OnEnable();

        //초기 위치 저장
        initPosition = officer.transform.position;

        //캐릭터 비활성화
        officer.gameObject.SetActive(false);
        balloon.SetActive(false);

        //경로 데이터 가져오기
        movePoints = movePoint.GetComponentsInChildren<Transform>();

        //애니메이터 설정
        //animator = GetComponent<Animator>();
        //animator = officer.GetComponentInChildren<Animator>();
    }

    protected override IEnumerator Playing()
    {
        officer.gameObject.SetActive(true);

        watchTime = Random.Range(minWatchTime, maxWatchTime);

        do
        {
            yield return null;

            //각 웨이포인트 이동
            float rest;

            for (int i = 0; i < movePoints.Length; i++)
            {
                //방향을 구한다
                A_direction dir = Utility.get2dDirection(officer.transform, movePoints[i]);
                
                //애니메이션 옵션 설정
                animator.SetInteger("Direction", (int)dir);
                animator.SetBool("Moving", true);

                do
                {
                    //이동하고 남은 거리를 구한다
                    rest = Utility.move2d(officer, movePoints[i], moveSpeed);
                    yield return null;

                } while (rest > 0);

                //정지 애니메이션으로 변환
                animator.SetBool("Moving", false);

                //목표 지점에서 감시 이벤트 시작
                if (i == checkPoint)
                {
                    //xx초동안 대기
                    do
                    {
                        watchTime -= Time.deltaTime;

                        if (DataManager.Instance.nowWorkStatus == WorkStatus.Resting)
                        {
                            Debug.Log("start");
                            //yield return new WaitForSeconds(1);
                            yield return StartCoroutine(Talking());
                            Debug.Log("end");
                            break;
                        }

                        yield return null;
                    } while (watchTime > 0);

                    //현재 일 상태 확인
                    //노는거 확인되면 잔소리 시작
                    //잔소리 끝나면 종료
                    //안놀면 종룟
                    //yield return new WaitForSeconds(1);
                }
            }



            yield return new WaitForSeconds(1);

            //상태 변경
            officer.gameObject.SetActive(false);
            officer.transform.position = initPosition;
            this.SetState(EventStatus.Waiting);

        } while (!isNewState);
    }


    IEnumerator Talking()
    {
        Debug.Log("start talking time");
        balloon.SetActive(true);
        yield return new WaitForSeconds(3);
        balloon.SetActive(false);
    }

}
