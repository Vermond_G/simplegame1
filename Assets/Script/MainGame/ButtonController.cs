﻿using UnityEngine;
using System.Collections;

//public enum WorkStatus { Working, Resting, Eventing }

public class ButtonController : MonoBehaviour
{
    //GuageController child;
    RequestEvent reqEvent;

    public GameObject computerOn;

    private string scoreKey = "score";

    void OnEnable()
    {
        DataManager.Instance.nowWorkStatus = WorkStatus.Resting;
        //child = gameObject.GetComponentInChildren<GuageController>();
        reqEvent = gameObject.GetComponentInChildren<RequestEvent>();

        StartCoroutine(checkMain());
    }

    //메인 코루틴. 
    IEnumerator checkMain()
    {
        while (true)
        {
            yield return StartCoroutine(DataManager.Instance.nowWorkStatus.ToString());
        }
    }

    public void SetStatus(WorkStatus status)
    {
        DataManager.Instance.nowWorkStatus = status;
    }


    public void onStartWork()
    {
        if (DataManager.Instance.isStop == true) return;

        if (DataManager.Instance.nowWorkStatus != WorkStatus.Eventing)
        {
            //작업중으로 상태 변환 
            SetStatus(WorkStatus.Working);
            computerOn.gameObject.SetActive(true);
        }
        else
        {
            //현재는 아무런 일도 하지 않는다
        }
    }

    public void onStopWork()
    {
        if (DataManager.Instance.isStop == true) return;

        if (DataManager.Instance.nowWorkStatus != WorkStatus.Eventing)
        {
            //휴식중으로 상태 변환 
            SetStatus(WorkStatus.Resting);
            computerOn.gameObject.SetActive(false);
        }
        else
        {
            //클릭 회수 증가
            reqEvent.currentButtonCount += 1;
        }
    }


    IEnumerator Working()
    {
        if (DataManager.Instance.isStop == false)
        {
            //float mRest = DataManager.Instance.nowRest;
            //float mWork = DataManager.Instance.nowWork;
            float mMaxWork = DataManager.Instance.characterData.maxWorkPoint;
            float bonus = DataManager.Instance.nowBonus;
            float convert = DataManager.Instance.characterData.workForRest;

            //작업중 동작 설정. 
            //휴식지수 감소, 감소한 양에 비례해 작업지수 증가 
            DataManager.Instance.nowRest -= 1;
            if (DataManager.Instance.nowRest <= 0) DataManager.Instance.nowRest = 0;

            if (DataManager.Instance.nowRest > 0)
            {
                DataManager.Instance.nowWork += convert * bonus;
                //작업 게이지 최대시 게임 종료
                if (DataManager.Instance.nowWork >= mMaxWork) //DataManager.Instance.nowWork = mMaxWork;
                {
                    //정확한 점수 체계가 정해지면 점수 설정할 것
                    int score = Random.Range(1, 100);

                    //데이터 전달을 위해 저장
                    PlayerPrefs.SetInt(scoreKey, score);

                    //점수 화면 불러오기
                    Application.LoadLevel("ScoreList");                    
                }
            }
        }

        yield return null;
    }

    IEnumerator Resting()
    {
        if (DataManager.Instance.isStop == false)
        {
            //float mRest = DataManager.Instance.nowRest;
            float mMaxRest = DataManager.Instance.characterData.maxRestPoint;

            //휴식중 동작 설정. 
            //휴식지수 증가 
            DataManager.Instance.nowRest += 1;
            if (DataManager.Instance.nowRest >= mMaxRest) DataManager.Instance.nowRest = mMaxRest;
        }

        yield return null;
    }

    IEnumerator Eventing()
    {
        yield return null;
    }

}