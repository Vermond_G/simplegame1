﻿using UnityEngine;
using System.Collections;

public class KeyInputController : MonoBehaviour
{
    public GameObject dialog;

    float original;

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            if (DataManager.Instance.isStop == false) StartCoroutine(PauseGame());
            //else StartCoroutine(ResumeGame());
        }
    }

    public void YesButton()
    {
        StartCoroutine(ResumeGame());
        Application.LoadLevel("StartScene");
    }

    public void NoButton()
    {
        StartCoroutine(ResumeGame());
    }

    IEnumerator PauseGame()
    {
        original = Time.timeScale;
        Time.timeScale = 0;
        DataManager.Instance.isStop = true;
        dialog.SetActive(true);

        yield return null;
    }

    IEnumerator ResumeGame()
    {
        Time.timeScale = original;
        DataManager.Instance.isStop = false;
        dialog.SetActive(false);

        yield return null;
    }

    
}
