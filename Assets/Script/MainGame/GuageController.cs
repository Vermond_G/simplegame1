﻿using UnityEngine;
using System.Collections;

using UnityEngine.UI;

public class GuageController : MonoBehaviour
{

    public Image workStatusBar;
    public Image restStatusBar;

    //현재 값.
    //public float mWork = 0;
    //public float mRest = 0;

    //최대 값.
    //public float mMaxRest = 100;
    //public float mMaxWork = 100;

    //보너스 배수
    //float bonusTime = 1;
    
    // Use this for initialization
    void OnEnable()
    {
    }

    // Update is called once per frame
    void Update()
    {
        //이미지 업데이트 코루틴 시작
        StartCoroutine(SetStatusBar());
    }

    IEnumerator SetStatusBar()
    {
        while (true)
        {
            //float mWork = DataManager.Instance.nowWork;
            //float mRest = DataManager.Instance.nowRest;
            //float mMaxWork = DataManager.Instance.characterData.maxWorkPoint;
            //float mMaxRest = DataManager.Instance.characterData.maxRestPoint;

            if (DataManager.Instance.isStop == false)
            {
                //work status bar 길이 업데이트
                workStatusBar.fillAmount = DataManager.Instance.nowWork / DataManager.Instance.characterData.maxWorkPoint;

                //rest status bar 업데이트
                restStatusBar.fillAmount = DataManager.Instance.nowRest / DataManager.Instance.characterData.maxRestPoint;
            }

            yield return null;
        }
    }

    //외부 접근 함수
}
