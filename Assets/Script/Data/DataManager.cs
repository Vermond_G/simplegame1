﻿using UnityEngine;
using System.Collections;

public enum WorkStatus { Working, Resting, Eventing }

public class DataManager : MonoBehaviour {
    //싱글톤 인스턴스
    private static DataManager _instance;

    //싱글톤 접근
    public static DataManager Instance
    {
        get { return _instance; }
    }

    //관리 객체
    public EventData eventData;
    public CharacterData characterData;

    //상태값
    public WorkStatus nowWorkStatus;
    public bool isStop = false;

    //게이지 상태
    public float nowWork = 0;
    public float nowRest = 0;
    public float nowBonus = 1;

    void Awake()
    {
        if (_instance == null)
        {
            _instance = this;
            eventData = (EventData) Resources.Load("Data/EventData");
            characterData = (CharacterData)Resources.Load("Data/CharacterData");
        }

    }

    //게이지가 최대인지 확인
    public bool isRestGuageMax()
    {
        if (nowRest >= characterData.maxRestPoint) return true;
        else return false;
    }



}
