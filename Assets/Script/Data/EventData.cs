﻿using UnityEngine;
using System.Collections;

public class EventData : ScriptableObject
{
    public Officer officer;
    public Request request;
    public PowerUp powerUp;

    [System.Serializable]
    public class EventBody
    {
        public float minWaitTime;
        public float maxWaitTime;
    }

    [System.Serializable]
    public class Officer : EventBody
    {
        public float minWatchTime;
        public float maxWatchTime;
    }

    [System.Serializable]
    public class Request : EventBody
    {
        public int minClickCount;
        public int maxClickCount;
    }

    [System.Serializable]
    public class PowerUp : EventBody
    {
        public float minGuagePercent;
        public float maxGuagePercent;

        public float bonusPercent;
    }
}
