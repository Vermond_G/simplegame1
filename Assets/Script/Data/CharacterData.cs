﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public class CharacterData : ScriptableObject {
    //최대 휴식 수치 / 작업 수치
    public float maxRestPoint;
    public float maxWorkPoint;

    //초당 회복되는 휴식 수치
    public float restForSecond;

    //휴식 수치 1 소모시 획득하는 작업 수치
    public float workForRest;
}
