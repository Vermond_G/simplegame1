﻿using UnityEngine;
using System.Collections;

//original : https://github.com/joaokucera/unity-procedural-tiled-background

public class TileBackground : MonoBehaviour
{
    public GameObject tilePrefab;
    public Camera tileCamera;

    public GameObject guiTilePrefab;
    public Camera mainCamera;

    /// <summary>
    /// Use this for initialization 
    /// </summary>
    void Awake()
    {
        if (tilePrefab.GetComponent<Renderer>() == null)
        {
            Debug.LogError("There is no renderer available to fill background.");
        }
        
        #region 사무실 바닥
        
        // tile size.
        Vector2 tileSize = tilePrefab.GetComponent<Renderer>().bounds.size;

        // set camera to orthographic.
        tileCamera.orthographic = true;

        // columns and rows.
        int columns = Mathf.CeilToInt(tileCamera.aspect * tileCamera.orthographicSize / tileSize.x);
        int rows = Mathf.CeilToInt(tileCamera.orthographicSize / tileSize.y);

        //비교를 위한 값 저장
        //+1은 보정치
        float minX = -columns * tileSize.x + tileSize.x / 2;
        float maxX = columns * tileSize.x + tileSize.x / 2;
        float minY = -rows * tileSize.y + tileSize.y / 2 + 1;
        float maxY = rows * tileSize.y + tileSize.y / 2 + 1; 

        // from screen left side to screen right side, because camera is orthographic.
        for (int c = -columns; c < columns; c++)
        {
            for (int r = -rows; r < rows; r++)
            {
                Vector2 position = new Vector2(c * tileSize.x + tileSize.x / 2, r * tileSize.y + tileSize.y / 2 + 1);

                GameObject tile = Instantiate(tilePrefab, position, Quaternion.identity) as GameObject;
                tile.transform.parent = transform;
            }
        }

        #endregion

        #region 배경 무늬

        // tile size.
        Vector2 guitileSize = guiTilePrefab.GetComponent<Renderer>().bounds.size;

        // set camera to orthographic.
        mainCamera.orthographic = true;

        // columns and rows.
        // 카메라 전체를 덮기 위해 넉넉하게 1.5배
        int guiColumns = Mathf.CeilToInt(mainCamera.aspect * mainCamera.orthographicSize / guitileSize.x * 1.5f);
        int guiRows = Mathf.CeilToInt(mainCamera.orthographicSize / guitileSize.y * 1.5f);

        // from screen left side to screen right side, because camera is orthographic.
        for (int c = -guiColumns; c < guiColumns; c++)
        {
            for (int r = -guiRows; r < guiRows; r++)
            {
                float x = c * guitileSize.x + guitileSize.x / 2;
                float y = r * guitileSize.y + guitileSize.y / 2;

                if ((x < minX || x > maxX) || (y < minY || y > maxY))
                {
                    Vector2 position = new Vector2(x, y);
                    GameObject tile = Instantiate(guiTilePrefab, position, Quaternion.identity) as GameObject;
                    tile.transform.parent = transform;
                }
            }
        }

        #endregion
    }
}
