﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using UnityEngine.UI;

public class ScoreList : MonoBehaviour 
{
    private int[] scoreList = new int[10];
    private string[] scoreNameList = new string[10];

    private string scoreKey = "score";
    private string scoreListKey = "scorelist";
    private string nameListKey = "namelist";
    private char token = '|';

    //점수 목록 출력
    public GameObject panel;
    //점수 오브젝트 원본 (복사할 원본)
    public GameObject scoreText;
    //이름 입력 다이얼로그
    public GameObject dialog;
    //이름을 가져올 오브젝트
    public GameObject nameInput;

    void Awake()
    {
        //우선 점수 목록을 가져온다
        LoadScoreList();

        int score = PlayerPrefs.GetInt(scoreKey);

        //점수가 0이라면 값이 없거나 필요없다는 뜻
        if (score != 0)
        {
            dialog.SetActive(true);
        }
        else
        {
            dialog.SetActive(false);
            InitList();
        }
    }

    void InitList()
    {
        Debug.Log("initList");
        //리스트 불러온다

        //리스트 초기화
        foreach (Transform child in panel.transform)
        {
            Destroy(child.gameObject);
        }

        //점수 아이템 추가
        for (int i = 0; i < scoreList.Length; i++)
        {
            //0점은 출력하지 않는다
            //if (scoreList[i] == 0) return;

            //프리팹 복제
            GameObject clone = Instantiate(scoreText);

            //점수 텍스트 설정
            Text testtext = clone.GetComponent<Text>();
            testtext.text = string.Format("{0}.\t\t{1}\t\t{2}", (i+1), scoreNameList[i], scoreList[i]);

            //위치 설정
            RectTransform rt = (RectTransform)(clone.transform);
            rt.localPosition = new Vector2(rt.localPosition.x, rt.localPosition.y - 50 * i);

            //활성화
            clone.SetActive(true);

            //부모를 지정해야 스크롤뷰 내부에 들어간다
            clone.transform.SetParent(panel.transform, false);
        }
    }

    //버튼 액션
    //새 점수 추가
    public void AddScore()
    {
        //점수 생성
        //실제로는 이전 씬에서 생성한 결과를 받아야 한다
        //테스트 버전에서는 랜덤 값을 사용한다
        int score = (int)Random.Range(1, 100);
        //int score = PlayerPrefs.GetInt(scoreKey);

        //읽은 뒤 무조건 삭제
        PlayerPrefs.DeleteKey(scoreKey);

        //이름 가져오기
        InputField nameField = nameInput.GetComponent<InputField>();
        string name = nameField.text;

        AddNewScore(score, name);
        SaveScore();
        dialog.SetActive(false);
        InitList();

        Debug.Log(PlayerPrefs.GetString(scoreListKey));
        Debug.Log(PlayerPrefs.GetString(nameListKey));
    }

    //버튼 액션
    //점수 초기화
    public void ResetScoreList()
    {
        ResetScore();
        InitList();
    }

    //테스트 함수
    //점수 목록만 봅시다
    public void OnlyShowList()
    {
        dialog.SetActive(false);
        InitList();
    }

    //새 점수 추가
    void AddNewScore(int score, string name)
    {

        for (int i = scoreList.Length - 1; i >= 0; i--)
        {
            Debug.Log(score + " : " + scoreList[i]);

            if (score >= scoreList[i])
            {
                Debug.Log("changed");

                if (i < scoreList.Length - 1)
                {
                    scoreList[i + 1] = scoreList[i];
                    scoreNameList[i + 1] = scoreNameList[i];
                }

                scoreList[i] = score;
                scoreNameList[i] = name;
            }
            else
            {
                break;
            }
        }
    }

    //점수 목록 저장
    void SaveScore()
    {
        StringBuilder sb = new StringBuilder();
        StringBuilder sb2 = new StringBuilder();

        sb.Append(scoreList[0]);
        sb2.Append(scoreNameList[0]);

        for (int i = 1; i < scoreList.Length; i++)
        {
            sb.Append(token);
            sb.Append(scoreList[i]);
            sb2.Append(token);
            sb2.Append(scoreNameList[i]);
        }

        PlayerPrefs.SetString(scoreListKey, sb.ToString());
        PlayerPrefs.SetString(nameListKey, sb2.ToString());
        PlayerPrefs.Save();
    }

    //점수 목록 불러오기
    void LoadScoreList()
    {
        Debug.Log("LoadScoreList");

        //점수 불러오기
        if (PlayerPrefs.HasKey(scoreListKey))
        {
            int score = 0;
            string[] list = PlayerPrefs.GetString(scoreListKey).Split(token);

            for (int i = 0; i < scoreList.Length; i++)
            {
                if (int.TryParse(list[i], out score))
                {
                    scoreList[i] = score;
                }
            }
        }
        else
        {
            for (int i = 0; i < 10; i++)
            {
                scoreList[i] = 0;
            }
        }

        //이름 불러오기
        if (PlayerPrefs.HasKey(nameListKey))
        {
            string[] list = PlayerPrefs.GetString(nameListKey).Split(token);

            for (int i = 0; i < scoreList.Length; i++)
            {
                scoreNameList[i] = list[i];
            }
        }
        else
        {
            for (int i = 0; i < 10; i++)
            {
                scoreNameList[i] = "";
            }
        }
    }

    //점수 목록 초기화
    void ResetScore()
    {
        for (int i = 0; i < 10; i++)
        {
            scoreList[i] = 0;
            scoreNameList[i] = "";
        }

        PlayerPrefs.DeleteKey(scoreListKey);
        PlayerPrefs.DeleteKey(nameListKey);
    }


}

