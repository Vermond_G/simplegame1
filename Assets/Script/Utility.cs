﻿using UnityEngine;
using System.Collections;

public class Utility {

    //오브젝트를 조금 움직이고 남은 거리를 반환한다
    public static float move2d(GameObject moveObject, Transform target, float moveSpeed)
    {
        Transform t = moveObject.transform;
        Vector2 targetPos = target.position;

        t.position = Vector2.MoveTowards(t.position, targetPos, moveSpeed * Time.deltaTime);
        
        return Vector2.Distance(t.position, targetPos);
    }

    //2D 좌표계에서 향하는 방향을 구한다
    public static A_direction get2dDirection(Transform from, Transform to)
    {
        float x = to.position.x - from.position.x;
        float y = to.position.y - from.position.y;

        if (Mathf.Abs(x) > Mathf.Abs(y))
        {
            if (x > 0) return A_direction.Right;
            else return A_direction.Left;
        }
        else
        {
            if (y > 0) return A_direction.Up;
            else return A_direction.Down;
        }
    }
}
