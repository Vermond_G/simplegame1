﻿using UnityEngine;
using System.Collections;

public class GUITile : MonoBehaviour {

    public Vector2 tilePosition;
    public Texture image;
    public float tileSize;
    public int tilesWide, tilesHigh;

    void OnGUI()
    {
        for (int y = 0; y < tilesHigh; y++)
            for (int x = 0; x < tilesWide; x++)
                GUI.DrawTexture(new Rect(tilePosition.x + (x * tileSize), tilePosition.y + (y * tileSize), tileSize, tileSize), image);
    }
	

    //void Awake()
    //{
    //    for (int y = 0; y < tilesHigh; y++)
    //        for (int x = 0; x < tilesWide; x++)
    //            GUI.DrawTexture(new Rect(tilePosition.x + (x * tileSize), tilePosition.y + (y * tileSize), tileSize, tileSize), image);
    //}
}
